# User Shortcut

- [Overview](#overview)
- [Installation](#installation)
- [Usage](#usage)
- [Acknowledgements](#acknowledgements)
- [Author](#author)

## Overview

User Shortcut is a replacement for Drupal Core's `shortcut` module that allows
users to manage their own sets of shortcuts. It does not remove core's shortcut
UIs, but instead integrates with them. This allows both modules to be installed
on a site, allowing users with the right permissions to switch between their own
private sets, or the global, shared sets created by administrators.

For users with the `maintain own shortcut sets` permission, a new tab called
"Private Shortcuts" is added on their user page. From there, they can create,
edit and delete as many Shortcut Sets as they want, and manage the links for
each of their sets, as well as select the set they want to show in the toolbar.

Users with access to the Drupal Core's `shortcut` module are still able to use
it. If they set one of their private sets as active, the private set will be
rendered for them. If they choose to use one of the global sets instead, then
that's what will be rendered.

## Installation

1. Include this module into your project via composer.

```
composer require drupal/user_shortcut
```

2. Install it.

```
drush pm:enable user_shortcut --yes
```

## Usage

This module requires the following permissions to be granted in order to allow
specific roles to use private shortcuts:

- `maintain own shortcut sets`
- `access toolbar`

## Acknowledgements

A big part of this module is based off the Drupal Core's `shortcut` module.

## Author

**Salvador Molina** (slv_)

