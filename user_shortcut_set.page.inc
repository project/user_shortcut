<?php

/**
 * @file
 * Contains user_shortcut_set.page.inc.
 *
 * Page callback for User Shortcut Set entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for User Shortcut Set templates.
 *
 * Default template: user_shortcut_set.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_user_shortcut_set(array &$variables) {
  // Fetch UserShortcutSet Entity Object.
  $user_shortcut_set = $variables['elements']['#user_shortcut_set'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
