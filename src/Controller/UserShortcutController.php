<?php

namespace Drupal\user_shortcut\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\user_shortcut\Entity\UserShortcutInterface;
use Drupal\user_shortcut\Entity\UserShortcutSetInterface;

/**
 * Main routes controller for User Shortcut entities.
 */
class UserShortcutController extends ControllerBase {

  /**
   * Returns a form to add a new shortcut to a given set.
   *
   * @param \Drupal\user_shortcut\Entity\UserShortcutSetInterface $user_shortcut_set
   *   The User Shortcut Set entity to which the new shortcut will be added.
   *
   * @return array
   *   The shortcut add form.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function addForm(UserShortcutSetInterface $user_shortcut_set) {
    $shortcut = $this->entityTypeManager()
      ->getStorage('user_shortcut')
      ->create(['user_shortcut_set' => $user_shortcut_set->id()]);
    return $this->entityFormBuilder()->getForm($shortcut, 'add');
  }

  /**
   * Deletes the selected shortcut.
   *
   * @param \Drupal\user_shortcut\Entity\UserShortcutInterface $shortcut
   *   The shortcut to delete.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect to the previous location or the front page when destination
   *   is not set.
   */
  public function deleteShortcutLinkInline(UserShortcutInterface $shortcut) {
    $label = $shortcut->label();

    try {
      $shortcut->delete();
      $this->messenger()->addStatus($this->t('The shortcut %title has been deleted.', ['%title' => $label]));
    }
    catch (\Exception $e) {
      $this->messenger()->addStatus($this->t('Unable to delete the shortcut for %title.', ['%title' => $label]), 'error');
    }

    return $this->redirect('<front>');
  }

}
