<?php

declare(strict_types = 1);

namespace Drupal\user_shortcut\Overrides\Toolbar;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\user_shortcut\Entity\UserShortcutSetInterface;
use Drupal\user_shortcut\UserShortcutSetActiveMap;

/**
 * Logic to render default 'shortcut' sets from core, or custom private sets.
 */
class ShortcutSetRenderer {

  /**
   * The account for which to render default shortcut sets, or private ones.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  private $account;

  /**
   * The User Shortcut Set Storage.
   *
   * @var \Drupal\user_shortcut\Entity\UserShortcutSetStorageInterface
   */
  private $userShortcutSetStorage;

  /**
   * The User Shortcut Set active map registry.
   *
   * @var \Drupal\user_shortcut\UserShortcutSetActiveMap
   */
  private $activeMap;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  private $renderer;

  /**
   * The entity repository.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  private $entityRepository;

  public function __construct(AccountInterface $account, EntityTypeManagerInterface $entityTypeManager, RendererInterface $renderer, EntityRepositoryInterface $entityRepository, UserShortcutSetActiveMap $activeMap) {
    $this->account = $account;
    $this->userShortcutSetStorage = $entityTypeManager->getStorage('user_shortcut_set');
    $this->renderer = $renderer;
    $this->entityRepository = $entityRepository;
    $this->activeMap = $activeMap;
  }

  public function alterToolbarContents(&$items) {
    if (!$active_set_id = $this->activeMap->getUserActiveSet((string) $this->account->id())) {
      return;
    }

    if (!$shortcut_set = $this->userShortcutSetStorage->load($active_set_id)) {
      return;
    }

    $items['shortcuts'] = [
      '#cache' => [
        'contexts' => [
          // Per-user cache, given each user maintains its own shortcuts.
          'user',
        ],
      ],
    ];

    if (!$this->account->hasPermission('maintain own shortcut sets')) {
      return;
    }

    $shortcuts = $this->renderShortcutSetLinks($shortcut_set);
    $this->renderer->addCacheableDependency($items['shortcuts'], $shortcut_set);

    $configure_link = [
      '#type' => 'link',
      '#title' => t('Edit shortcuts'),
      '#url' => Url::fromRoute('entity.user_shortcut_set.customize_form', [
        'user_shortcut_set' => $shortcut_set->id(),
      ]),
      '#options' => ['attributes' => ['class' => ['edit-shortcuts']]],
    ];

    $items['shortcuts'] += [
      '#type' => 'toolbar_item',
      'tab' => [
        '#type' => 'link',
        '#title' => t('Shortcuts'),
        '#url' => $shortcut_set->toUrl('collection'),
        '#attributes' => [
          'title' => t('Shortcuts'),
          'class' => ['toolbar-icon', 'toolbar-icon-shortcut'],
        ],
      ],
      'tray' => [
        '#heading' => t('User-defined shortcuts'),
        'shortcuts' => $shortcuts,
        'configure' => $configure_link,
      ],
      '#weight' => -10,
      '#attached' => [
        'library' => [
          'user_shortcut/drupal.user_shortcut',
        ],
      ],
    ];
  }

  /**
   * @param \Drupal\user_shortcut\Entity\UserShortcutSetInterface $shortcut_set
   *
   * @return array
   */
  private function renderShortcutSetLinks(UserShortcutSetInterface $shortcut_set) {
    $shortcut_links = [];
    $cache_tags = [];
    foreach ($shortcut_set->getShortcuts() as $shortcut) {
      $shortcut = $this->entityRepository->getTranslationFromContext($shortcut);
      $url = $shortcut->getUrl();
      if ($url->access()) {
        $links[$shortcut->id()] = [
          'type' => 'link',
          'title' => $shortcut->label(),
          'url' => $shortcut->getUrl(),
        ];
        $cache_tags = Cache::mergeTags($cache_tags, $shortcut->getCacheTags());
      }
    }

    if (!empty($links)) {
      $shortcut_links = [
        '#theme' => 'links__toolbar_shortcuts',
        '#links' => $links,
        '#attributes' => [
          'class' => ['toolbar-menu'],
        ],
        '#cache' => [
          'tags' => $cache_tags,
        ],
      ];
    }

    return $shortcut_links;
  }

}
