<?php

declare(strict_types = 1);

namespace Drupal\user_shortcut;

use Drupal\Core\Database\Connection;

/**
 * Class UserShortcutSetActiveMap.
 */
class UserShortcutSetActiveMap {

  const TABLE = 'user_shortcut_set_active_map';

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  private $db;

  /**
   * UserShortcutSetActiveMap constructor.
   */
  public function __construct(Connection $db) {
    $this->db = $db;
  }

  /**
   * @param string $userId
   * @param string $shortcutSetId
   */
  public function setUserActiveSet(string $userId, string $shortcutSetId) {
    $this->db->upsert(self::TABLE)
      ->key('uid')
      ->fields(['uid', 'user_shortcut_set'], [$userId, $shortcutSetId])
      ->execute();
  }

  /**
   * @param string $userId
   * @param string $shortcutSetId
   */
  public function getUserActiveSet(string $userId) {
    // @todo add static cache.
    $query = $this->db->query("SELECT user_shortcut_set FROM {" . self::TABLE . "} 
      WHERE uid = :uid", [':uid' => $userId]);

    if ($set_id = $query->fetchField()) {
      return $set_id;
    }
    return NULL;
  }

  /**
   * Deletes the shortcut set status info for a given account.
   */
  public function deleteActiveSetInformationForUser(string $userId) {
    $this->db->delete(self::TABLE)
      ->where('uid = :uid', [':uid' => $userId])
      ->execute();
  }

  /**
   * Deletes the status information for a given shortcut set.
   */
  public function deleteSetStatusInfo(string $shortcutSetId) {
    $this->db->delete(self::TABLE)
      ->where('user_shortcut_set = :set_name', [':set_name' => $shortcutSetId])
      ->execute();
  }

}
