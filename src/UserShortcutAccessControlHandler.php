<?php

namespace Drupal\user_shortcut;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityHandlerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user_shortcut\Entity\UserShortcutInterface;
use Drupal\user_shortcut\Entity\UserShortcutSetStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the access control handler for the user shortcut entity type.
 *
 * @see \Drupal\user_shortcut\Entity\Shortcut
 */
class UserShortcutAccessControlHandler extends EntityAccessControlHandler implements EntityHandlerInterface {

  /**
   * The shortcut_set storage.
   *
   * @var \Drupal\user_shortcut\Entity\UserShortcutSetStorageInterface
   */
  protected $shortcutSetStorage;

  /**
   * Constructs a ShortcutAccessControlHandler object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\user_shortcut\Entity\UserShortcutSetStorageInterface $shortcut_set_storage
   *   The shortcut_set storage.
   */
  public function __construct(EntityTypeInterface $entity_type, UserShortcutSetStorageInterface $shortcut_set_storage) {
    parent::__construct($entity_type);
    $this->shortcutSetStorage = $shortcut_set_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage('user_shortcut_set')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /* @var \Drupal\user_shortcut\Entity\UserShortcutInterface $entity */
    if (!$shortcut_set = $entity->getParentSet()) {
      return AccessResult::neutral()->addCacheableDependency($entity);
    }

    if ($shortcut_set->getOwnerId() == $account->id()) {
      return AccessResult::allowed()->addCacheableDependency($entity);
    }

    return AccessResult::neutral()->addCacheableDependency($entity);
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $shortcut_set_id = NULL) {
    /* @var \Drupal\user_shortcut\Entity\UserShortcutSetInterface $shortcut_set */
    if (!$shortcut_set = $this->shortcutSetStorage->load($shortcut_set_id)) {
      // @todo: Should this return forbidden instead?
      return AccessResult::neutral();
    }

    if ($shortcut_set->getOwner()->id() == $account->id()) {
      return AccessResult::allowed();
    }
    return AccessResult::allowedIfHasPermission($account, 'administer user shortcut set entities');
  }

}
