<?php

namespace Drupal\user_shortcut;

use Drupal\Core\Entity\EntityListBuilderInterface;

/**
 * Defines an interface for User Shortcut Set entities list builders.
 */
interface UserShortcutSetListBuilderInterface extends EntityListBuilderInterface {

  /**
   * Builds a listing of user shortcut sets that belong to the given user.
   *
   * @param string $uid
   *   The user id.
   *
   * @return array
   *   A render array as expected by
   *   \Drupal\Core\Render\RendererInterface::render().
   */
  public function renderForUser(string $uid);

}
