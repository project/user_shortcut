<?php

namespace Drupal\user_shortcut;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of User Shortcut Set entities.
 */
class UserShortcutSetListBuilder extends EntityListBuilder implements UserShortcutSetListBuilderInterface {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity) {
    $operations = parent::getDefaultOperations($entity);

    if (isset($operations['edit'])) {
      $operations['edit']['title'] = t('Edit shortcut set');
    }

    $operations['list'] = [
      'title' => t('List links'),
      'url' => $entity->toUrl('customize-form'),
    ];

    return $operations;
  }

  /**
   * {@inheritdoc}
   */
  public function renderForUser(string $uid) {
    $build['table'] = [
      '#type' => 'table',
      '#header' => $this->buildHeader(),
      '#title' => $this->getTitle(),
      '#rows' => [],
      '#empty' => $this->t('There are no @label yet.', ['@label' => $this->entityType->getPluralLabel()]),
      '#cache' => [
        'contexts' => $this->entityType->getListCacheContexts(),
        'tags' => $this->entityType->getListCacheTags(),
      ],
    ];

    $sets = $this->loadUserShortcutSets($uid);
    foreach ($sets as $entity) {
      if ($row = $this->buildRow($entity)) {
        $build['table']['#rows'][$entity->id()] = $row;
      }
    }

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $build['pager'] = [
        '#type' => 'pager',
      ];
    }
    return $build;
  }

  /**
   * Loads entity IDs using a pager sorted by the entity id.
   *
   * @param string $uid
   *   The user id.
   *
   * @return \Drupal\user_shortcut\Entity\UserShortcutSetInterface[]|array
   *   An array of entity IDs.
   */
  protected function loadUserShortcutSets(string $uid): array {
    $query = $this->getStorage()->getQuery()
      ->condition('user_id', $uid)
      ->sort($this->entityType->getKey('id'));

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $query->pager($this->limit);
    }

    if ($ids = $query->execute()) {
      return $this->storage->loadMultiple(array_values($ids));
    }

    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\user_shortcut\Entity\UserShortcutSet $entity */
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.user_shortcut_set.customize_form',
      ['user_shortcut_set' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
