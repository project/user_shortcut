<?php

namespace Drupal\user_shortcut\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\user_shortcut\UserShortcutSetActiveMap;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a storage for shortcut_set entities.
 */
class UserShortcutSetStorage extends SqlContentEntityStorage implements UserShortcutSetStorageInterface {

  /**
   * The user shortcut set active map.
   *
   * @var \Drupal\user_shortcut\UserShortcutSetActiveMap
   */
  private $activeMapRegistry;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    $instance = new static(
      $entity_type,
      $container->get('database'),
      $container->get('entity_field.manager'),
      $container->get('cache.entity'),
      $container->get('language_manager'),
      $container->get('entity.memory_cache'),
      $container->get('entity_type.bundle.info'),
      $container->get('entity_type.manager')
    );
    $instance->setActiveMapRegistry($container->get('user_shortcut.registry.active_map'));
    return $instance;
  }

  /**
   * Setter injection method for the active map registry.
   *
   * @param \Drupal\user_shortcut\UserShortcutSetActiveMap $activeMap
   *   The active map registry.
   */
  private function setActiveMapRegistry(UserShortcutSetActiveMap $activeMap) {
    $this->activeMapRegistry = $activeMap;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteAssignedShortcuts(UserShortcutSetInterface $entity) {
    $shortcuts = $entity->getShortcuts();
    $user_shortcut_storage = $this->entityTypeManager->getStorage('user_shortcut');
    $user_shortcut_storage->delete($shortcuts);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteSetInfoFromActiveMap(UserShortcutSetInterface $entity) {
    $this->activeMapRegistry->deleteSetStatusInfo($entity->id());
  }

}
