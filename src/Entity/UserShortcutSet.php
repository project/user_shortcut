<?php

namespace Drupal\user_shortcut\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the User Shortcut Set entity.
 *
 * @ingroup user_shortcut
 *
 * @ContentEntityType(
 *   id = "user_shortcut_set",
 *   label = @Translation("User Shortcut Set"),
 *   handlers = {
 *     "storage" = "Drupal\user_shortcut\Entity\UserShortcutSetStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\user_shortcut\UserShortcutSetListBuilder",
 *     "views_data" = "Drupal\user_shortcut\Entity\UserShortcutSetViewsData",
 *     "translation" = "Drupal\user_shortcut\UserShortcutSetTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\user_shortcut\Form\UserShortcutSetForm",
 *       "add" = "Drupal\user_shortcut\Form\UserShortcutSetForm",
 *       "edit" = "Drupal\user_shortcut\Form\UserShortcutSetForm",
 *       "customize" = "Drupal\user_shortcut\Form\UserShortcutSetCustomizeForm",
 *       "delete" = "Drupal\user_shortcut\Form\UserShortcutSetDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\user_shortcut\UserShortcutSetHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\user_shortcut\UserShortcutSetAccessControlHandler",
 *   },
 *   base_table = "user_shortcut_set",
 *   data_table = "user_shortcut_set_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer user shortcut set entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *   },
 *   links = {
 *     "canonical" = "/content/user_shortcut/set/{user_shortcut_set}",
 *     "add-form" = "/content/user_shortcut/set/add",
 *     "edit-form" = "/content/user_shortcut/set/{user_shortcut_set}/edit",
 *     "customize-form" = "/content/user_shortcut/set/{shortcut_set}/customize",
 *     "delete-form" = "/content/user_shortcut/set/{user_shortcut_set}/delete",
 *     "collection" = "/content/user_shortcut/set",
 *   },
 *   field_ui_base_route = "user_shortcut_set.settings"
 * )
 */
class UserShortcutSet extends ContentEntityBase implements UserShortcutSetInterface {

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function preDelete(EntityStorageInterface $storage, array $entities) {
    parent::preDelete($storage, $entities);

    /* @var \Drupal\user_shortcut\Entity\UserShortcutSetInterface[] $entities */
    foreach ($entities as $entity) {
      $storage->deleteAssignedShortcuts($entity);
      $storage->deleteSetInfoFromActiveMap($entity);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the User Shortcut Set entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the User Shortcut Set entity.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getShortcuts() {
    $shortcut_storage = $this->entityTypeManager()->getStorage('user_shortcut');

    $shortcut_ids = $shortcut_storage->getQuery()
      ->condition('user_shortcut_set', $this->id())
      ->sort('weight')
      ->sort('title')
      ->execute();

    if (!empty($shortcut_ids)) {
      return $shortcut_storage->loadMultiple(array_values($shortcut_ids));
    }
    return [];
  }

}
