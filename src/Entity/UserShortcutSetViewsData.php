<?php

namespace Drupal\user_shortcut\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for User Shortcut Set entities.
 */
class UserShortcutSetViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
