<?php

namespace Drupal\user_shortcut\Entity;

use Drupal\Core\Entity\ContentEntityStorageInterface;

/**
 * Defines an interface for shortcut_set entity storage classes.
 */
interface UserShortcutSetStorageInterface extends ContentEntityStorageInterface {

  /**
   * Deletes user shortcuts that were created as part of the given shortcut set.
   *
   * @param \Drupal\user_shortcut\Entity\UserShortcutSetInterface $entity
   *   Delete the user assigned sets belonging to this shortcut.
   */
  public function deleteAssignedShortcuts(UserShortcutSetInterface $entity);

}
