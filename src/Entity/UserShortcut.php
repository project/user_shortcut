<?php

namespace Drupal\user_shortcut\Entity;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\link\LinkItemInterface;

/**
 * Defines the shortcut entity class.
 *
 * @property \Drupal\link\LinkItemInterface link
 *
 * @ContentEntityType(
 *   id = "user_shortcut",
 *   label = @Translation("Shortcut link"),
 *   label_collection = @Translation("Shortcut links"),
 *   label_singular = @Translation("shortcut link"),
 *   label_plural = @Translation("shortcut links"),
 *   label_count = @PluralTranslation(
 *     singular = "@count shortcut link",
 *     plural = "@count shortcut links",
 *   ),
 *   handlers = {
 *     "access" = "Drupal\user_shortcut\UserShortcutAccessControlHandler",
 *     "form" = {
 *       "default" = "Drupal\user_shortcut\Form\UserShortcutForm",
 *       "add" = "Drupal\user_shortcut\Form\UserShortcutForm",
 *       "edit" = "Drupal\user_shortcut\Form\UserShortcutForm",
 *       "delete" = "Drupal\user_shortcut\Form\UserShortcutDeleteForm"
 *     },
 *     "translation" = "Drupal\content_translation\ContentTranslationHandler"
 *   },
 *   base_table = "user_shortcut",
 *   data_table = "user_shortcut_field_data",
 *   translatable = TRUE,
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "label" = "title",
 *     "langcode" = "langcode",
 *   },
 *   links = {
 *     "canonical" = "/content/user_shortcut/link/{shortcut}",
 *     "delete-form" = "/content/user_shortcut/link/{shortcut}/delete",
 *     "edit-form" = "/content/user_shortcut/link/{shortcut}",
 *   },
 *   list_cache_tags = { "config:user_shortcut_set_list" },
 * )
 */
class UserShortcut extends ContentEntityBase implements UserShortcutInterface {

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->get('title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle($link_title) {
    $this->set('title', $link_title);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight() {
    return $this->get('weight')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setWeight($weight) {
    $this->set('weight', $weight);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getUrl() {
    return $this->link->first()->getUrl();
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);

    // If it's not an update, then this shortcut was just created.
    // Invalidate parent set cache tags as well.
    if (!$update) {
      Cache::invalidateTags($this->getCacheTagsToInvalidate());
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    /** @var \Drupal\Core\Field\BaseFieldDefinition[] $fields */
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['id']->setDescription(t('The ID of the shortcut.'));

    $fields['uuid']->setDescription(t('The UUID of the shortcut.'));

    $fields['user_shortcut_set'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel($entity_type->getBundleLabel())
      ->setSetting('target_type', 'user_shortcut_set')
      ->setRequired(TRUE)
      ->setReadOnly(TRUE);

    $fields['langcode']->setDescription(t('The language code of the shortcut.'));

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the shortcut.'))
      ->setRequired(TRUE)
      ->setTranslatable(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -10,
        'settings' => [
          'size' => 40,
        ],
      ]);

    $fields['weight'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Weight'))
      ->setDescription(t('Weight among shortcuts in the same shortcut set.'));

    $fields['link'] = BaseFieldDefinition::create('link')
      ->setLabel(t('Path'))
      ->setDescription(t('The location this shortcut points to.'))
      ->setRequired(TRUE)
      ->setSettings([
        'link_type' => LinkItemInterface::LINK_INTERNAL,
        'title' => DRUPAL_DISABLED,
      ])
      ->setDisplayOptions('form', [
        'type' => 'link_default',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getParentSet(): UserShortcutSetInterface {
    return $this->get('user_shortcut_set')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTagsToInvalidate() {
    return $this->getParentSet()->getCacheTags();
  }

}
