<?php

namespace Drupal\user_shortcut\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining User Shortcut Set entities.
 *
 * @ingroup user_shortcut
 */
interface UserShortcutSetInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the User Shortcut Set name.
   *
   * @return string
   *   Name of the User Shortcut Set.
   */
  public function getName();

  /**
   * Sets the User Shortcut Set name.
   *
   * @param string $name
   *   The User Shortcut Set name.
   *
   * @return \Drupal\user_shortcut\Entity\UserShortcutSetInterface
   *   The called User Shortcut Set entity.
   */
  public function setName($name);

  /**
   * Gets the User Shortcut Set creation timestamp.
   *
   * @return int
   *   Creation timestamp of the User Shortcut Set.
   */
  public function getCreatedTime();

  /**
   * Sets the User Shortcut Set creation timestamp.
   *
   * @param int $timestamp
   *   The User Shortcut Set creation timestamp.
   *
   * @return \Drupal\user_shortcut\Entity\UserShortcutSetInterface
   *   The called User Shortcut Set entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns all the shortcuts from a shortcut set sorted correctly.
   *
   * @return \Drupal\user_shortcut\Entity\UserShortcut[]
   *   An array of user shortcut entities.
   */
  public function getShortcuts();

}
