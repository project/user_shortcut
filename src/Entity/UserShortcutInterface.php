<?php

declare(strict_types = 1);

namespace Drupal\user_shortcut\Entity;

/**
 * Provides an interface for defining User Shortcut entities.
 */
interface UserShortcutInterface {

  /**
   * Returns the title of this user shortcut.
   *
   * @return string
   *   The title of this user shortcut.
   */
  public function getTitle();

  /**
   * Sets the title of this user shortcut.
   *
   * @param string $title
   *   The title of this user shortcut.
   *
   * @return \Drupal\user_shortcut\Entity\UserShortcutInterface
   *   The called shortcut entity.
   */
  public function setTitle($title);

  /**
   * Returns the weight among shortcuts with the same depth.
   *
   * @return int
   *   The shortcut weight.
   */
  public function getWeight();

  /**
   * Sets the weight among shortcuts with the same depth.
   *
   * @param int $weight
   *   The shortcut weight.
   *
   * @return \Drupal\user_shortcut\Entity\UserShortcutInterface
   *   The called shortcut entity.
   */
  public function setWeight($weight);

  /**
   * Returns the URL object pointing to the configured route.
   *
   * @return \Drupal\Core\Url
   *   The URL object.
   */
  public function getUrl();

  /**
   * Returns this shortcut's parent UserShortcutSet entity.
   *
   * @return \Drupal\user_shortcut\Entity\UserShortcutSetInterface
   *   The parent User Shortcut Set.
   */
  public function getParentSet(): UserShortcutSetInterface;

}
