<?php

namespace Drupal\user_shortcut;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the User Shortcut Set entity.
 *
 * @see \Drupal\user_shortcut\Entity\UserShortcutSet.
 */
class UserShortcutSetAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\user_shortcut\Entity\UserShortcutSetInterface $entity */
    switch ($operation) {
      case 'view':
      case 'update':
        if ($entity->getOwnerId() == $account->id()) {
          return AccessResult::allowedIfHasPermission($account, 'maintain own shortcut sets');
        }
        return AccessResult::allowedIfHasPermission($account, 'administer user shortcut set entities');

      case 'delete':
        if ($entity->getOwnerId() == $account->id()) {
          return AccessResult::allowedIfHasPermission($account, 'maintain own shortcut sets')->addCacheableDependency($entity);
        }
        return AccessResult::allowedIfHasPermission($account, 'administer user shortcut set entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'maintain own shortcut sets');
  }

}
