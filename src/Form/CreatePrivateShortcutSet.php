<?php

namespace Drupal\user_shortcut\Form;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\UserInterface;
use Drupal\user_shortcut\Entity\UserShortcutSetStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Builds the create user shortcut set form.
 */
class CreatePrivateShortcutSet extends FormBase {

  /**
   * The account the shortcut set is for.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * The shortcut set storage.
   *
   * @var \Drupal\user_shortcut\Entity\UserShortcutSetStorageInterface
   */
  protected $shortcutSetStorage;

  /**
   * Constructs a CreatePrivateShortcutSet object.
   *
   * @param \Drupal\user_shortcut\Entity\UserShortcutSetStorageInterface $shortcutSetStorage
   *   The shortcut set storage.
   */
  public function __construct(UserShortcutSetStorageInterface $shortcutSetStorage) {
    $this->shortcutSetStorage = $shortcutSetStorage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')->getStorage('user_shortcut_set')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, UserInterface $user = NULL) {
    $this->user = $user;

    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#required' => TRUE,
    ];
    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Create set'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /* @var \Drupal\user_shortcut\Entity\UserShortcutSetInterface $set */
    $set = $this->shortcutSetStorage->create([
      'name' => $form_state->getValue('name'),
      'user_id' => $this->user->id(),
    ]);
    $set->save();

    $this->messenger()->addStatus($this->t('%set_name shortcut set created.
     You can edit it from this page.',
      [
        '%set_name' => $set->label(),
      ]));

    $form_state->setRedirect('user_shortcut.user.set_switch', [
      'user' => $this->user->id(),
    ]);
  }

  /**
   * Checks access for the shortcut set switch form.
   *
   * @param \Drupal\user\UserInterface $user
   *   (optional) The user creating the shortcut set.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function checkAccess(UserInterface $user = NULL) {
    $user_can_maintain_own_sets = $user->hasPermission('maintain own shortcut sets');
    if ($user_can_maintain_own_sets && $this->currentUser()->id() === $user->id()) {
      return AccessResult::allowed();
    }

    return AccessResult::forbidden();
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'user_shortcut_create_private_shortcut_set';
  }

}
