<?php

namespace Drupal\user_shortcut\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

/**
 * Builds the user shortcut set customize form.
 */
class UserShortcutSetCustomizeForm extends ContentEntityForm {

  /**
   * The entity being used by this form.
   *
   * @var \Drupal\user_shortcut\Entity\UserShortcutSet
   */
  protected $entity;

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state, $param = NULL) {
    $form['shortcuts'] = [
      '#tree' => TRUE,
      '#weight' => -20,
    ];


    $route = \Drupal::routeMatch()->getRouteObject();
    $is_admin_route = \Drupal::service('router.admin_context')->isAdminRoute($route);
//    $route->getOptions()['parameters']['param']['type']


    // @todo: Doable passing a param from services file?
    if ($is_admin_route) {
      $add_shortcut_link = $this->getUrlGenerator()->generateFromRoute('entity.user_shortcut.link_add',
        [
          'user_shortcut_set' => $this->entity->id(),
        ]);
    }
    else {
      $add_shortcut_link = $this->getUrlGenerator()->generateFromRoute('user_shortcut.user.link_add',
        [
          'user' => $this->entity->getOwnerId(),
          'user_shortcut_set' => $this->entity->id(),
        ]);
    }
    $form['shortcuts']['links'] = [
      '#type' => 'table',
      '#header' => [t('Name'), t('Weight'), t('Operations')],
      '#empty' => $this->t('No shortcuts available. <a href=":link">Add a shortcut</a>', [':link' => $add_shortcut_link]),
      '#attributes' => ['id' => 'shortcuts'],
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'shortcut-weight',
        ],
      ],
    ];

    foreach ($this->entity->getShortcuts() as $shortcut) {
      $id = $shortcut->id();
      $url = $shortcut->getUrl();
      if (!$url->access()) {
        continue;
      }
      $form['shortcuts']['links'][$id]['#attributes']['class'][] = 'draggable';
      $form['shortcuts']['links'][$id]['name'] = [
        '#type' => 'link',
        '#title' => $shortcut->getTitle(),
      ] + $url->toRenderArray();
      unset($form['shortcuts']['links'][$id]['name']['#access_callback']);
      $form['shortcuts']['links'][$id]['#weight'] = $shortcut->getWeight();
      $form['shortcuts']['links'][$id]['weight'] = [
        '#type' => 'weight',
        '#title' => t('Weight for @title', ['@title' => $shortcut->getTitle()]),
        '#title_display' => 'invisible',
        '#default_value' => $shortcut->getWeight(),
        '#attributes' => ['class' => ['shortcut-weight']],
      ];

      $links['edit'] = [
        'title' => t('Edit'),
        'url' => $shortcut->toUrl(),
      ];
      $links['delete'] = [
        'title' => t('Delete'),
        'url' => $shortcut->toUrl('delete-form'),
      ];
      $form['shortcuts']['links'][$id]['operations'] = [
        '#type' => 'operations',
        '#links' => $links,
        '#access' => $url->access(),
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    // Only includes a Save action for the entity, no direct Delete button.
    return [
      'submit' => [
        '#type' => 'submit',
        '#value' => t('Save'),
        '#access' => (bool) Element::getVisibleChildren($form['shortcuts']['links']),
        '#submit' => ['::submitForm', '::save'],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    foreach ($this->entity->getShortcuts() as $shortcut) {
      $weight = $form_state->getValue(['shortcuts', 'links', $shortcut->id(), 'weight']);
      $shortcut->setWeight($weight);
      $shortcut->save();
    }
    $this->messenger()->addStatus($this->t('The shortcut set has been updated.'));
  }

}
