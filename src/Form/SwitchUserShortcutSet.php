<?php

namespace Drupal\user_shortcut\Form;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\UserInterface;
use Drupal\user_shortcut\Entity\UserShortcutSetInterface;
use Drupal\user_shortcut\UserShortcutSetActiveMap;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Builds the user shortcut set switch form.
 *
 * @internal
 */
class SwitchUserShortcutSet extends FormBase {

  /**
   * The account the shortcut set is for.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * The user shortcut set storage.
   *
   * @var \Drupal\user_shortcut\Entity\UserShortcutSetStorageInterface
   */
  protected $shortcutSetStorage;

  /**
   * The user shortcut set active map registry.
   *
   * @var \Drupal\user_shortcut\UserShortcutSetActiveMap
   */
  private $activeMap;

  /**
   * The list builder service for user shortcut sets.
   *
   * @var \Drupal\user_shortcut\UserShortcutSetListBuilderInterface
   */
  private $listBuilder;

  /**
   * Constructs a SwitchShortcutSet object.
   *
   * @param \Drupal\Core\Entity\ContentEntityStorageInterface $shortcut_set_storage
   *   The shortcut set storage.
   */
  public function __construct(ContentEntityStorageInterface $shortcut_set_storage, UserShortcutSetActiveMap $activeMap) {
    $this->shortcutSetStorage = $shortcut_set_storage;
    $this->activeMap = $activeMap;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')->getStorage('user_shortcut_set'),
      $container->get('user_shortcut.registry.active_map')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'user_shortcut_set_switch';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, UserInterface $user = NULL) {
    $this->user = $user;

    // Prepare the list of shortcut sets.
    $options = array_map(function (UserShortcutSetInterface $set) {
      return $set->label();
    }, $this->shortcutSetStorage->loadByProperties(['user_id' => $this->user->id()]));

    $current_set_id = $this->activeMap->getUserActiveSet($this->user->id());
    $this->listBuilder = \Drupal::entityTypeManager()->getListBuilder('user_shortcut_set');
    $form['list'] = $this->listBuilder->renderForUser($this->user->id());

    if (!$options) {
      return $form;
    }

    $form['set'] = [
      '#type' => 'radios',
      '#title' => $this->t('Choose a set of shortcuts to use'),
      '#options' => $options,
      '#default_value' => $current_set_id,
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Change set'),
    ];

    return $form;
  }

  /**
   * Determines if a shortcut set exists already.
   *
   * @param string $id
   *   The set ID to check.
   *
   * @return bool
   *   TRUE if the shortcut set exists, FALSE otherwise.
   */
  public function exists($id) {
    return (bool) $this->shortcutSetStorage->getQuery()
      ->condition('id', $id)
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->getValue('set') == 'new') {
      // Check to prevent creating a shortcut set with an empty title.
      if (trim($form_state->getValue('label')) == '') {
        $form_state->setErrorByName('label', $this->t('The new set label is required.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $account = $this->currentUser();

    $account_is_user = $this->user->id() == $account->id();
    // Switch to a different shortcut set.
    /* @var \Drupal\user_shortcut\Entity\UserShortcutSetInterface $set */
    $set = $this->shortcutSetStorage->load($form_state->getValue('set'));
    $replacements = [
      '%user' => $this->user->getDisplayName(),
      '%set_name' => $set->label(),
    ];

    // Set as active and save, if it's not the currently active.
    $this->activeMap->setUserActiveSet($this->user->id(), $set->id());
    $this->messenger()->addStatus($account_is_user ? $this->t('You are now using the %set_name shortcut set.', $replacements) : $this->t('%user is now using the %set_name shortcut set.', $replacements));
  }

  /**
   * Checks access for the shortcut set switch form.
   *
   * @param \Drupal\user\UserInterface $user
   *   (optional) The owner of the shortcut set.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function checkAccess(UserInterface $user = NULL) {
    if ($user->id() == $this->currentUser()->id()) {
      return AccessResult::allowedIfHasPermission($user, 'maintain own shortcut sets')
        ->cachePerPermissions()
        ->cachePerUser();
    }
    return AccessResult::forbidden("user can't manage other's users shortcut sets.")
      ->cachePerUser();
  }

}
