<?php

namespace Drupal\user_shortcut\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;
use Drupal\Core\Url;

/**
 * Provides a form for deleting User Shortcut Set entities.
 *
 * @ingroup user_shortcut
 */
class UserShortcutSetDeleteForm extends ContentEntityDeleteForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'user_shortcut_set_confirm_delete';
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.user_shortcut_set.customize_form', [
      'user_shortcut_set' => $this->entity->id(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  protected function getRedirectUrl() {
    return new Url('user_shortcut.user.set_switch', [
      'user' => $this->entity->getOwnerId(),
    ]);
  }

}
