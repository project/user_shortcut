<?php

namespace Drupal\user_shortcut;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for user_shortcut_set.
 */
class UserShortcutSetTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
